import hashlib


def encrypt(normalString):
    hashedString = hashlib.sha256(normalString.encode('ascii')).hexdigest()
    #print(hashedString)
    return hashedString

encrypt('apple')