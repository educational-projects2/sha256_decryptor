from threading import Thread
import pickle
import time
start_time = time.time()
#print(start_time)
#Decryptor.bulkhead('1115dd800feaacefdf481f1f9070374a2a81e27880f187396db67958b207cbad')
#Decryptor.bulkhead('3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b')
#Decryptor.bulkhead('74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f')


def check(dict, hash):
    for key in dict:
        #print(threading.current_thread().getName(), " ", key)
        if dict[key] == hash:
            print("password is ", key)
            #threading.current_thread().join()
            #print(time.time() - start_time)
            return
        #await


def split_dict(input_dict: dict, num_parts: int) -> list:
    list_len: int = len(input_dict)
    return [dict(list(input_dict.items())[i * list_len // num_parts:(i + 1) * list_len // num_parts])
        for i in range(num_parts)]


def multithreadedSearch(threadCount, hash):
    with open("mySavedDict.txt", "rb") as myFile:
        passDictionary = pickle.load(myFile)
    splittedDict = split_dict(passDictionary, threadCount)
    th = []
    for i in range(0, threadCount):
        th.append(Thread(target=check, args=(splittedDict[i], hash)))
        th[i].name = "Thread %i" % i
        th[i].start()


multithreadedSearch(3, '1115dd800feaacefdf481f1f9070374a2a81e27880f187396db67958b207cbad')
#multithreadedSearch(3, '3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b')
#multithreadedSearch(3, '74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f')
print("-----------------", time.time() - start_time)

#1 - 24.20548439025879
#3 - 26.72387933731079